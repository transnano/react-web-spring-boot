package com.example.reactwebspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactWebSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactWebSpringBootApplication.class, args);
	}

}
